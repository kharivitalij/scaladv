# scaladv
scaladv - restful api to manage advertisers.

Dependencies:

* [finch](https://github.com/finagle/finch)

* [doobie](https://tpolecat.github.io/doobie/)
## How to run:
##### Clone this repo:
```
$ git clone https://kharivitalij@bitbucket.org/kharivitalij/scaladv.git
```
##### Download and install sbt: [sbt](https://www.scala-sbt.org/download.html)
##### Run using sbt:
```
$ cd scaladv
$ sbt run
```
##### Or using Docker:
```
$ cd scaladv
$ docker build -t scaladv .
$ docker run --network host scaladv
```
It will run server at [localhost:8081](localhost:8081)

Database already contains some values, you can check it - 
[localhost:8081/api/advertiser/get/all](localhost:8081/api/advertiser/get/all)

## How to test:
```
$ cd scaladv
$ sbt test
```
It will create scaladv-test database for test values and than remove it.
## How to configure:
You can provide your custom values(e.g. database configuration) in src/main/resources/application.conf file.