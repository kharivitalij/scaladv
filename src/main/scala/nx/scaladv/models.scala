package nx.scaladv

case class Advertiser(
    name: String,
    contactName: String,
    creditLimit: Double,
    id: Int
)