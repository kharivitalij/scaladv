package nx.scaladv

import io.finch._


object decorators {
  def withInternalServerError[T](func: => Output[T]): Output[T] = {
    try {
      func
    }
    catch {
      case _: Throwable => InternalServerError(new Exception("Something went wrong on server:("))
    }
  }
}
