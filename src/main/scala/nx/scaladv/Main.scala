package nx.scaladv


import com.twitter.finagle.Http
import com.twitter.util.Await
import io.finch._
import io.finch.circe._
import io.circe.generic.auto._


object Main extends App {

  val endpoints = new Endpoints("scaladv")

  val api = endpoints.getById :+:
            endpoints.getAll :+:
            endpoints.add :+:
            endpoints.update :+:
            endpoints._delete :+:
            endpoints.validate :+:
            endpoints.deduct

  Await.ready(
    Http.server
      .serve(":8081", api.toServiceAs[Application.Json])
  )
}