package nx.scaladv

import doobie._
import doobie.implicits._
import cats.effect.IO
import scala.concurrent.ExecutionContext

import com.typesafe.config.ConfigFactory


class DbService(configBase: String) {

  implicit val cs = IO.contextShift(ExecutionContext.global)

  val xa = Transactor.fromDriverManager[IO](
    ConfigFactory.load().getString(s"$configBase.db.driver"),
    ConfigFactory.load().getString(s"$configBase.db.url"),
    ConfigFactory.load().getString(s"$configBase.db.user"),
    ConfigFactory.load().getString(s"$configBase.db.password")
  )

  sql"""create table if not exists advertiser
        (
        name varchar(255) not null,
        contactName varchar(255) not null,
        creditLimit decimal not null,
        id integer not null AUTO_INCREMENT,
        primary key(id)
        );
    """.update.run.transact(xa).unsafeRunSync

  def getById(id: Int): Option[Advertiser] =
    sql"select * from advertiser where id = $id"
      .query[Advertiser]
      .option
      .transact(xa)
      .unsafeRunSync

  def getAll(): List[Advertiser] =
    sql"select * from advertiser"
      .query[Advertiser]
      .to[List]
      .transact(xa)
      .unsafeRunSync

  def add(name: String, contactName: String, creditLimit: Double): Int = {
    val id = for {
      id <- sql"insert into advertiser (name, contactName, creditLimit) values($name, $contactName, $creditLimit)"
        .update
        .withUniqueGeneratedKeys[Int]("id")
      p <- sql"select id from advertiser where id = $id".query[Int].unique
    } yield p
    id.transact(xa).unsafeRunSync
  }

  def update(id: Int, name: String, contactName: String, creditLimit: Double) = {
    sql"update advertiser set name=$name, contactName=$contactName, creditLimit=$creditLimit where id = $id"
      .update
      .run
      .transact(xa)
      .unsafeRunSync
  }

  def delete(id: Int) = {
    sql"delete from advertiser where id = $id"
      .update
      .run
      .transact(xa)
      .unsafeRunSync
  }
}