package nx.scaladv

import cats.effect.IO
import io.finch._
import io.finch.circe._
import io.finch.catsEffect._
import io.circe.generic.auto._
import decorators.withInternalServerError


class Endpoints(configBase: String) {

  val rootUrl = "api" :: "advertiser"

  val dbService = new DbService(configBase)

  def getById: Endpoint[IO, Advertiser] = get(rootUrl :: "get" :: path[Int]) { id: Int =>
    withInternalServerError[Advertiser] {
      val adv = dbService.getById(id)
      adv match {
        case None => NoContent
        case Some(adv: Advertiser) => Ok(adv)
      }
    }
  }

  def getAll: Endpoint[IO, List[Advertiser]] = get(rootUrl :: "get" :: "all") {
    withInternalServerError[List[Advertiser]] {
      val advs = dbService.getAll
      advs match {
        case Nil => NoContent
        case _ => Ok(advs)
      }
    }
  }

  def add: Endpoint[IO, Boolean] = post(rootUrl :: "add" :: jsonBody[Advertiser]) { adv: Advertiser =>
    withInternalServerError[Boolean] {
      adv.creditLimit < 0 match {
        case true => BadRequest(new IllegalArgumentException("You can't add advertiser with credit limit < 0"))
        case false =>
          val id = dbService.add(adv.name, adv.contactName, adv.creditLimit)
          Created(true).withHeader("Location" -> s"/api/advertiser/get/$id")
      }
    }
  }

  def update: Endpoint[IO, Boolean] = put(rootUrl :: "update" :: path[Int] :: jsonBody[Advertiser]) {
    (id: Int, adv: Advertiser) =>
      withInternalServerError[Boolean] {
        val _adv = dbService.getById(id)
        _adv match {
          case None => NoContent
          case Some(_adv: Advertiser) =>
            adv.creditLimit < 0 match {
              case true => BadRequest(new IllegalArgumentException("You can't update advertiser with credit limit < 0"))
              case false =>
                dbService.update(id, adv.name, adv.contactName, adv.creditLimit)
                Ok(true).withHeader("Location" -> s"/api/advertiser/get/$id")
            }
        }
      }
  }

  def _delete: Endpoint[IO, Boolean] = delete(rootUrl :: "delete" :: path[Int]) { id: Int =>
    withInternalServerError[Boolean] {
      val adv = dbService.getById(id)
      adv match {
        case None => NoContent
        case Some(_adv: Advertiser) =>
          dbService.delete(id)
          Ok(true)
      }
    }
  }

  def validate: Endpoint[IO, Boolean] = get(rootUrl :: "validate" :: path[Int] :: path[String]) { (id: Int, s: String) =>
    withInternalServerError[Boolean] {
      val sum = s.toDouble
      dbService.getById(id) match {
        case None => BadRequest(new IllegalArgumentException("There is not advertiser with id = $id"))
        case Some(adv: Advertiser) => Ok(validateCreditLimit(adv, sum))
      }
    }
  }

  def validateCreditLimit(adv: Advertiser, sum: Double): Boolean = {
    adv.creditLimit >= sum
  }

  def deduct: Endpoint[IO, Boolean] = post(rootUrl :: "deduct" :: path[Int] :: path[String]) { (id: Int, s: String) =>
    withInternalServerError[Boolean] {
      val sum = s.toDouble
      val adv = dbService.getById(id)
      adv match {
        case None => BadRequest(new IllegalArgumentException("There is not advertiser with id = $id"))
        case Some(adv: Advertiser) =>
          if (validateCreditLimit(adv, sum)) {
            dbService.update(id, adv.name, adv.contactName, adv.creditLimit - sum)
            Ok(true).withHeader("Location" -> s"/api/advertiser/get/$id")
          }
          else BadRequest(new IllegalArgumentException("Operation permitted, advertiser haven't enough money"))
      }
    }
  }
}
