package nx.scaladv


import io.finch.circe._
import io.circe.generic.auto._
import io.finch._
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import com.twitter.finagle.http.{Status, HeaderMap}


class MainTest extends FunSuite with BeforeAndAfterAll {

  val endpoints = new Endpoints("scaladv-test")

  test("testGetAll") {
    assert(endpoints.getAll(Input.get("/api/advertiser/get/all"))
      .awaitOutputUnsafe().map(_.status).contains(Status.NoContent))
  }

  test("testAddStatus") {
    assert(endpoints.add(Input.post("/api/advertiser/add").withBody[Application.Json](
      Advertiser("name", "contactName", 500, 0))).awaitOutputUnsafe().map(_.status).contains(Status.Created))
    assert(endpoints.add(Input.post("/api/advertiser/add").withBody[Application.Json](
      Advertiser("name2", "contactName2", 1000, 0))).awaitOutputUnsafe().map(_.status).contains(Status.Created))
    assert(endpoints.add(Input.post("/api/advertiser/add").withBody[Application.Json](
      Advertiser("name2", "contactName2", -100, 0))).awaitOutputUnsafe().map(_.status).contains(Status.BadRequest))
  }

//  test("testAddHeaders") {
//    assert(endpoints.add(Input.post("/api/advertiser/add").withBody[Application.Json](
//      Advertiser("name", "contactName", 500, 0))).awaitOutputUnsafe().map(_.headers)
  //      .contains("Location" -> "/api/advertiser/get/2"))
//  }

  test("testGetByIdSuccess") {
    val adv = endpoints.getById(Input.get("/api/advertiser/get/1")).awaitOutputUnsafe().map(_.value)
    adv match {
      case None => assert(false)
      case Some(adv: Advertiser) =>
        assert(adv.name == "name")
        assert(adv.contactName == "contactName")
        assert(adv.creditLimit == 500)
        assert(adv.id == 1)
    }

    val adv2 = endpoints.getById(Input.get("/api/advertiser/get/2")).awaitOutputUnsafe().map(_.value)
    adv2 match {
      case None => assert(false)
      case Some(adv: Advertiser) =>
        assert(adv.name == "name2")
        assert(adv.contactName == "contactName2")
        assert(adv.creditLimit == 1000)
        assert(adv.id == 2)
    }

    assert(endpoints.getById(Input.get("/api/advertiser/get/200"))
      .awaitOutputUnsafe().map(_.status).contains(Status.NoContent))
  }

  test("testGetAll2") {
    val output = endpoints.getAll(Input.get("/api/advertiser/get/all")).awaitOutputUnsafe()
    assert(output.map(_.status).contains(Status.Ok))
    val advs = output.map(_.value)
    advs match {
      case None => assert(false)
      case Some(advs: List[Advertiser]) =>
        assert(advs.length == 2)
    }
  }

  test("testUpdate") {
    assert(endpoints.update(Input.put("/api/advertiser/update/1").withBody[Application.Json](
      Advertiser("name3", "contactName3", 700, 0))).awaitOutputUnsafe().map(_.status).contains(Status.Ok))
    assert(endpoints.update(Input.put("/api/advertiser/update/100").withBody[Application.Json](
      Advertiser("name3", "contactName3", 700, 0))).awaitOutputUnsafe().map(_.status).contains(Status.NoContent))
    assert(endpoints.update(Input.put("/api/advertiser/update/1").withBody[Application.Json](
      Advertiser("name3", "contactName3", -200, 0))).awaitOutputUnsafe().map(_.status).contains(Status.BadRequest))

    val adv = endpoints.getById(Input.get("/api/advertiser/get/1")).awaitOutputUnsafe().map(_.value)
    adv match {
      case None => assert(false)
      case Some(adv: Advertiser) =>
        assert(adv.name == "name3")
        assert(adv.contactName == "contactName3")
        assert(adv.creditLimit == 700)
        assert(adv.id == 1)
    }
  }

  test("testDelete") {
    val advsBefore = endpoints.getAll(Input.get("/api/advertiser/get/all")).awaitValueUnsafe()
    advsBefore match {
      case None => assert(false)
      case Some(advs: List[Advertiser]) =>
        assert(advs.length == 2)
    }

    assert(endpoints._delete(Input.delete("/api/advertiser/delete/100"))
      .awaitOutputUnsafe().map(_.status).contains(Status.NoContent))
    assert(endpoints._delete(Input.delete("/api/advertiser/delete/2"))
      .awaitOutputUnsafe().map(_.status).contains(Status.Ok))

    val advsAfter = endpoints.getAll(Input.get("/api/advertiser/get/all")).awaitValueUnsafe()
    advsAfter match {
      case None => assert(false)
      case Some(advs: List[Advertiser]) =>
        assert(advs.length == 1)
    }
  }

  test("testValidate") {
    assert(endpoints.validate(Input.get("/api/advertiser/validate/1/100.67"))
      .awaitOutputUnsafe().map(_.status).contains(Status.Ok))
    assert(endpoints.validate(Input.get("/api/advertiser/validate/1/kg10"))
      .awaitOutputUnsafe().map(_.status).contains(Status.InternalServerError))
    assert(endpoints.validate(Input.get("/api/advertiser/validate/1/10.a"))
      .awaitOutputUnsafe().map(_.status).contains(Status.InternalServerError))
  }

  test("testDeduct") {
    val advBefore = endpoints.getById(Input.get("/api/advertiser/get/1")).awaitValueUnsafe()
    advBefore match {
      case None => assert(false)
      case Some(adv: Advertiser) =>
        assert(adv.creditLimit == 700)
    }

    assert(endpoints.deduct(Input.post("/api/advertiser/deduct/1/99999"))
      .awaitOutputUnsafe().map(_.status).contains(Status.BadRequest))
    assert(endpoints.deduct(Input.post("/api/advertiser/deduct/199/9"))
      .awaitOutputUnsafe().map(_.status).contains(Status.BadRequest))
    assert(endpoints.deduct(Input.post("/api/advertiser/deduct/1/9.we4g"))
      .awaitOutputUnsafe().map(_.status).contains(Status.InternalServerError))
    assert(endpoints.deduct(Input.post("/api/advertiser/deduct/1/30.345"))
      .awaitOutputUnsafe().map(_.status).contains(Status.Ok))

    val advAfter = endpoints.getById(Input.get("/api/advertiser/get/1")).awaitValueUnsafe()
    advAfter match {
      case None => assert(false)
      case Some(adv: Advertiser) =>
        assert(adv.creditLimit == 700-30.345)
    }
  }

  override def afterAll() = {
    import java.io.File

    val paths = List("./db/scaladv-test.mv.db", "./db/scaladv-test.trace.db")
    for (path <- paths) {
      val fileTemp = new File(path)
      if (fileTemp.exists) {
        fileTemp.delete()
      }
    }
  }
}